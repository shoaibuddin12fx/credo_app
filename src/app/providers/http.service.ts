import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { GeneralService } from "./general.service";
import { GlobaldataService } from "./globaldata.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class HttpService {
  headers: any;

  //globalUrl = "http://stagging.nsol.sg/cloudrexpo/public/api/";
  // globalUrl = "https://cloudrexpo.com/api/";

  // globalUrl = "http://stagging.nsol.sg/erp/public/api/";
  // globalUrl = "https://www.noohbuilders.com/credo/public/api/";
  // globalUrl = "https://erp.nsol.sg/api/";
  BASE_PATH = "http://credo.waapsdeveloper.com";
  globalUrl = `${this.BASE_PATH}/api/`;

  api = {
    login: "user/login",
    signup: "signup",
    forgotpassword: "forgotpassword",
    getstudents: "students/index",
    getstudentProfile: "students/profile",
    setstudentProfile: "students/profile/update",
    getsinglestudent: "students/show/",
    getCounsellors: 'students/counselor/list',
    setCounsellor: 'students/counselor/update',
    setpicture: "students/profile/setpicture",
    addcounsellor: "user/addcounsellor",

    storestudentaffair: "student_affairs/store",
    studentAffairsByStudentId: "student_affairs/student_affairs_by_student_id",
    storeacademicaffairs: "micro_performance_academics_department/store",
    academicAffairsByStudentId: "micro_performance_academics_department/academics_department_by_student_id",
    storecounsellingaffairs: "micro_performance_counselling_department/store",
    counsellingAffairsByStudent: "micro_performance_counselling_department/counselling_department_by_student_id",

    academicsDepartmentApproveReject: "micro_performance_academics_department/approve-reject",
    studentAffairsApproveReject: "student_affairs/approve-reject",
    counsellingDepartmentApproveReject: "micro_performance_counselling_department/approve-reject",

    privacypolicy: "privacypolicy",
    packages: "packages",
    buypackage: "buypackage",
    upgradepackage: "upgradepackage",
    userpackageledger: "userpackageledger",
    userpackagedetails: "userpackagedetails",
    howitworks: "howitworks",
    myreferrals: "myreferrals",
    categories: "categories",
    terms: "termsandconditions",
    zoomdetails: "zoomdetails",
    gettimezone: "gettimezone",
    gettimeslots: "gettimeslots",
    consultation: "consultation",
    countrydata: "countrydata",
    specialdeals: "specialdeals",
    products: "products",
    featuredprojects: "featuredprojects",
    sponsoredprojects: "sponsoredprojects",
    faqs: "faqs",
    contact: "contact",
    productdetails: "productdetails",
    editprofile: "editprofile",
    changepassword: "changepassword",
    uploadavatar: "uploadavatar",
    profiledetails: "profiledetails",
    ycssharemsg: "ycssharemsg",
    cities: "cities",
    pricerange: "pricerange",
    searchprojects: "searchprojects",

  };
  constructor(
    private http: HttpClient,
    public general: GeneralService,
    private router: Router
  ) { }

  postApi(link, data, loader) {
    if (loader == true) {
      this.general.presentLoading();
    }
    this.headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + GlobaldataService.token,
      Accept: "application/json",
    };
    return new Promise((resolve) => {
      this.http
        .post(this.globalUrl + link, JSON.stringify(data), {
          headers: this.headers,
        })
        .subscribe(
          (data) => {
            resolve(data);
            if (loader == true) {
              this.general.stopLoading();
            }
          },
          (error) => {
            console.log("Error" + error);
            this.general.stopLoading();
            if (error.status == 0) {
              this.general.presentToast(error.error.message);
            } else if (error.status == 401) {
              this.general.presentToast(error.error.message);
            } else if (error.status == 500) {
              this.general.presentToast(error.status);
            } else if (error.status == 503) {
              this.general.presentToast("Server Error");
            } else if (error.status == 404) {
              console.log(error);
              this.general.presentToast("Error: " + error.status);
            } else if (error.status == 400) {
              this.general.presentToast(error.status);
            } else if (error.status == 409) {
              this.general.presentToast(error.status);
            } else if (error.status == 406) {
              this.general.presentToast(error.status);
            } else {
              this.general.presentToast(error.status);
            }
          }
        );
    });
  }

  postApi2(link, data, loader) {
    if (loader == true) {
      this.general.presentLoading();
    }
    this.headers = { "Content-Type": "application/json" };
    return new Promise((resolve) => {
      this.http
        .post(this.globalUrl + link, JSON.stringify(data), {
          headers: this.headers,
        })
        .subscribe(
          (data) => {
            resolve(data);
            if (loader == true) {
              this.general.stopLoading();
            }
          },
          (error) => {
            console.log("Error" + error);
            this.general.stopLoading();
            if (error.status == 0) {
              this.general.presentToast(error.error.message);
            } else if (error.status == 500) {
              this.general.presentToast(error.status);
            } else if (error.status == 503) {
              this.general.presentToast("Server Error");
            } else if (error.status == 401) {
              console.log(error);
              this.general.presentToast("Error: " + error.status);
            } else if (error.status == 400) {
              this.general.presentToast(error.status);
            } else if (error.status == 409) {
              this.general.presentToast(error.status);
            } else if (error.status == 406) {
              this.general.presentToast(error.status);
            } else {
              this.general.presentToast(error.status);
            }
          }
        );
    });
  }

  getApi(link, loader) {
    if (loader == true) {
      this.general.presentLoading();
    }
    this.headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + GlobaldataService.token,
      Accept: "application/json",
    };
    return new Promise((resolve) => {
      this.http.get(this.globalUrl + link, { headers: this.headers }).subscribe(
        (data) => {
          resolve(data);
          this.general.stopLoading();
        },
        (error) => {
          console.log("Error" + error);
          this.general.stopLoading();
          if (error.status == 0) {
            this.general.presentToast(error.error.message);
          } else if (error.status == 500) {
            this.general.presentToast(error.status);
          } else if (error.status == 503) {
            this.general.presentToast("Server Error");
          } else if (error.status == 401) {
            console.log(error);

            this.general.presentToast("Error: " + error.status);
          } else if (error.status == 400) {
            this.general.presentToast(error.status);
          } else if (error.status == 409) {
            this.general.presentToast(error.status);
          } else if (error.status == 406) {
            this.general.presentToast(error.status);
          } else {
            this.general.presentToast(error.status);
          }
        }
      );
    });
  }

  Test(link, loader, myparams) {
    if (loader == true) {
      this.general.presentLoading();
    }
    this.headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + GlobaldataService.token,
      Accept: "application/json",
    };
    return new Promise((resolve) => {
      this.http
        .get(this.globalUrl + link, { headers: this.headers, params: myparams })
        .subscribe(
          (data) => {
            resolve(data);
            this.general.stopLoading();
          },
          (error) => {
            console.log("Error" + error);
            this.general.stopLoading();
            if (error.status == 0) {
              this.general.presentToast(error.error.message);
            } else if (error.status == 500) {
              this.general.presentToast(error.status);
            } else if (error.status == 503) {
              this.general.presentToast("Server Error");
            } else if (error.status == 401) {
              console.log(error);

              this.general.presentToast("Error: " + error.status);
            } else if (error.status == 400) {
              this.general.presentToast(error.status);
            } else if (error.status == 409) {
              this.general.presentToast(error.status);
            } else if (error.status == 406) {
              this.general.presentToast(error.status);
            } else {
              this.general.presentToast(error.status);
            }
          }
        );
    });
  }

  pagination(link, loader) {
    if (loader == true) {
      this.general.presentLoading();
    }
    this.headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + GlobaldataService.token,
      Accept: "application/json",
    };
    return new Promise((resolve) => {
      this.http.get(link, { headers: this.headers }).subscribe(
        (data) => {
          resolve(data);
          this.general.stopLoading();
        },
        (error) => {
          console.log("Error" + error);
          this.general.stopLoading();
          if (error.status == 0) {
            this.general.presentToast(error.error.message);
          } else if (error.status == 500) {
            this.general.presentToast(error.status);
          } else if (error.status == 503) {
            this.general.presentToast("Server Error");
          } else if (error.status == 401) {
            console.log(error);
            this.general.presentToast("Error: " + error.status);
          } else if (error.status == 400) {
            this.general.presentToast(error.status);
          } else if (error.status == 409) {
            this.general.presentToast(error.status);
          } else if (error.status == 406) {
            this.general.presentToast(error.status);
          } else {
            this.general.presentToast(error.status);
          }
        }
      );
    });
  }

  getApi2(link) {
    this.headers = { "Content-Type": "application/json" };
    return new Promise((resolve) => {
      this.http.get(this.globalUrl + link, { headers: this.headers }).subscribe(
        (data) => {
          resolve(data);
        },
        (err) => {
          console.log("Error" + err);
        }
      );
    });
  }
  getFlag(link, loader) {
    if (loader == true) {
      this.general.presentLoading();
    }
    this.headers = {
      "Content-Type": "application/json",
    };
    return new Promise((resolve) => {
      this.http.get(link, { headers: this.headers }).subscribe(
        (data) => {
          resolve(data);
          this.general.stopLoading();
        },
        (error) => {
          this.general.stopLoading();
          console.log("Error" + error);
        }
      );
    });
  }
}
