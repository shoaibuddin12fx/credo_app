import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class GlobaldataService {
  constructor() {}

  public static userObject: any;
  public static token: any;
  public static exhibitionstatus: any;
  public static selectedService: any;
  public static locations = [];
  public static deviceToken: any;
  public static historySelect: any;
  public static currentPage: any;
  public static userName: any;
  public static platform: any;
  public static netcheck: boolean = true;
  public static isloggedin: boolean;
  public static cardname: any;
}
