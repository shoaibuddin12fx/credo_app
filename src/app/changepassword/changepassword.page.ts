import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { HttpService } from "../providers/http.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import { GeneralService } from "../providers/general.service";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-changepassword",
  templateUrl: "./changepassword.page.html",
  styleUrls: ["./changepassword.page.scss"],
})
export class ChangepasswordPage implements OnInit {
  formgroup: FormGroup;
  oldpassword: AbstractControl;
  newpassword: AbstractControl;
  confirmpassword: AbstractControl;
  isSubmitted: boolean = false;
  validate_confirmpassword: any;
  chkCP: boolean = false;
  constructor(
    private router: Router,
    private storage: Storage,
    private general: GeneralService,
    private http: HttpService,
    public formbuilder: FormBuilder,
    private location: Location
  ) {
    this.formgroup = formbuilder.group({
      oldpassword: ["", [Validators.required]],
      newpassword: ["", [Validators.required, Validators.minLength(6)]],
      confirmpassword: ["", [Validators.required, Validators.minLength(6)]],
    });

    this.oldpassword = this.formgroup.controls["oldpassword"];
    this.newpassword = this.formgroup.controls["newpassword"];
    this.confirmpassword = this.formgroup.controls["confirmpassword"];
  }

  goback() {
    this.location.back();
  }

  checkConfirmPassword(t) {
    this.validate_confirmpassword = "";
    // console.log(this.password , this.confirmPassword ,t, t.target.value, t.target.value)
    if (t.target.value.length != 0) {
      if (t.target.value === this.newpassword.value) {
        // console.log("Password matched")
        this.validate_confirmpassword = "";
        this.chkCP = true;
      } else {
        // console.log("Password donot match")
        this.validate_confirmpassword = "Passwords donot match";
        this.chkCP = false;
      }
    } else {
      // this.validate_confirmpassword = MyApp.allFieldsReqText
      this.chkCP = false;
    }
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get("newpassword");
    const { value: confirmPassword } = formGroup.get("confirmpassword");
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

  submitForm() {
    this.isSubmitted = true;
    console.log(this.formgroup.value);
    if (this.formgroup.valid && this.chkCP == true) {
      var service_data = {
        currentpassword: this.oldpassword.value,
        password: this.newpassword.value,
        confirm_password: this.confirmpassword.value,
      };
      this.http
        .postApi(this.http.api.changepassword, service_data, true)
        .then((res: any) => {
          console.log(res);
          if (res.success == true) {
            this.general.presentToast(res.message);
            this.storage.clear();
            this.router.navigate(["/login"]);
          } else {
            this.general.presentToast(res.message);
          }
        });
    }
  }

  ngOnInit() {}
}
