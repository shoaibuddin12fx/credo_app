import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CounsellingaffairsPageRoutingModule } from './counsellingaffairs-routing.module';

import { CounsellingaffairsPage } from './counsellingaffairs.page';
import { CounsellingAffairsComponentModule } from '../components/counselling-affairs/counselling-affairs.component.module';
import { AcademicAffairsComponentModule } from '../components/academic-affairs/academic-affairs.component.module';
import { StudentaffairsComponentModule } from '../components/student-affairs/student-affairs.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CounsellingaffairsPageRoutingModule,
    StudentaffairsComponentModule,
    AcademicAffairsComponentModule,
    CounsellingAffairsComponentModule
  ],
  declarations: [CounsellingaffairsPage]
})
export class CounsellingaffairsPageModule { }
