import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { HttpService } from "../providers/http.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { GeneralService } from "../providers/general.service";
import { AlertController } from "@ionic/angular";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-counsellingaffairs",
  templateUrl: "./counsellingaffairs.page.html",
  styleUrls: ["./counsellingaffairs.page.scss"],
})
export class CounsellingaffairsPage implements OnInit {

  toggle3 = true;
  inOtherPage = false;

  loading_sa = false;
  loading_ac = false;
  loading_co = false;

  submitted_sa = false;
  submitted_ac = false;
  submitted_co = false;

  id_sa = -1;
  id_ac = -1;
  id_co = -1;




  constructor(
    private locationapp: Location,
    private http: HttpService,
    private general: GeneralService,
  ) {

  }

  ngOnInit(): void {
    this.inOtherPage = (GlobaldataService.userObject.department_name == 'Admin' || GlobaldataService.userObject.department_name == "Management") ? true : false;
    console.log(GlobaldataService.userObject.department_name);
  }

  goback() {
    this.locationapp.back();
  }

  sendId($event){
    console.log($event);

    switch($event.flag){
      case 'sa':
        this.id_sa = $event.id;
      break;
      case 'ac':
        this.id_ac = $event.id;
      break;
      case 'co':
        this.id_co = $event.id;
      break;

    }
  }

  hitPost($event, flag, param){

    $event.stopPropagation();

    let url = ''
    switch(param){
      case 'sa':
        this.loading_sa = true;
        url = this.http.api.studentAffairsApproveReject + "/" + this.id_sa;

      break;
      case 'ac':
        this.loading_ac = true;
        url = this.http.api.academicsDepartmentApproveReject + "/" + this.id_ac;

      break;
      case 'co':
        this.loading_co = true;
        url = this.http.api.counsellingDepartmentApproveReject + "/" + this.id_co;

      break;

    }


    this.http
      .postApi(url, { approved: flag }, false)
      .then((res: any) => {
        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);



        }

        if(this.loading_sa == true){
          this.submitted_sa = true
        }

        if(this.loading_ac == true){
          this.submitted_ac = true;
        }

        if(this.loading_co == true){
          this.submitted_co = true;
        }


        this.loading_sa = false;
        this.loading_ac = false;
        this.loading_co = false;
      });


  }

}
