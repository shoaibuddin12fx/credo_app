import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounsellingaffairsPage } from './counsellingaffairs.page';

const routes: Routes = [
  {
    path: '',
    component: CounsellingaffairsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CounsellingaffairsPageRoutingModule {}
