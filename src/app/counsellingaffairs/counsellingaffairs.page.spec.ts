import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CounsellingaffairsPage } from './counsellingaffairs.page';

describe('CounsellingaffairsPage', () => {
  let component: CounsellingaffairsPage;
  let fixture: ComponentFixture<CounsellingaffairsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounsellingaffairsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CounsellingaffairsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
