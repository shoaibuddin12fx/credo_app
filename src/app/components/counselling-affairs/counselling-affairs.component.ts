import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { GeneralService } from 'src/app/providers/general.service';
import { HttpService } from 'src/app/providers/http.service';

@Component({
  selector: 'app-counselling-affairs',
  templateUrl: './counselling-affairs.component.html',
  styleUrls: ['./counselling-affairs.component.scss'],
})
export class CounsellingAffairsComponent implements OnInit {
  platform: any = 'android';
  fullname: any;
  @Input() inOtherPage: boolean = false;
  @Input() fname: any;
  @Input() lname: any;
  @Input() id: any;
  @Input() student_id: any;
  @Output('sendId') sendId: EventEmitter<any> = new EventEmitter<any>();
  formgroup: FormGroup;
  number: AbstractControl;
  counsellor: AbstractControl;
  reasons: any = [];
  reason_student: any;
  student_remarks: any;
  parent_concern: any;
  studentData: any;
  formData: any;
  reason_for_under_performance: any;
  weekly_counselling: any;
  meet_the_teacher: any;
  peer_tutoring: any;
  meet_the_parents: any;
  associate_teacher: any;
  recommendations: any;
  isSubmitted: boolean = false;
  loading = false;
  date = null;

  constructor(
    public alertController: AlertController,
    private http: HttpService,
    private route: ActivatedRoute,
    private general: GeneralService
  ) {}

  submit() {
    let subjects = [];

    let error = false;


    this.formData.forEach(element => {

      if(!element.final_follow_up || element.final_follow_up == ''){
        this.general.presentToast('Final follow up must be filled');
        error = true;
      }

      if(!element.final_follow_up_out_come || element.final_follow_up_out_come == ''){
        this.general.presentToast('Final follow up out come must be filled');
        error = true;
      }



    });


    if(error){
      return;
    }


    this.formData.forEach((element) => {
      subjects.push({
        student_subject_id: element.student_subject_id,
        reason_for_under_performance: element.reason_for_under_performance
          ? element.reason_for_under_performance
          : '',
        weekly_counselling:
          element.weekly_counselling !== undefined
            ? element.weekly_counselling
            : false,
        meet_the_teacher:
          element.meet_the_teacher !== undefined
            ? element.meet_the_teacher
            : false,
        peer_tutoring:
          element.peer_tutoring !== undefined ? element.peer_tutoring : false,
        meet_the_parents:
          element.meet_the_parents !== undefined
            ? element.meet_the_parents
            : false,
        associate_teacher:
          element.associate_teacher !== undefined
            ? element.associate_teacher
            : false,
        recommendations:
          element.recommendations !== undefined
            ? element.recommendations
            : false,
        first_follow_up: element.first_follow_up ? element.first_follow_up : '',
        first_follow_up_out_come: element.first_follow_up_out_come
          ? element.first_follow_up_out_come
          : '',
        second_follow_up: element.second_follow_up
          ? element.second_follow_up
          : '',
        second_follow_up_out_come: element.second_follow_up_out_come
          ? element.second_follow_up_out_come
          : '',
        third_follow_up: element.third_follow_up ? element.third_follow_up : '',
        third_follow_up_out_come: element.third_follow_up_out_come
          ? element.third_follow_up_out_come
          : '',
        final_follow_up: element.final_follow_up ? element.final_follow_up : '',
        final_follow_up_out_come: element.final_follow_up
          ? element.final_follow_up_out_come
          : '',
      });
    });
    let data = {
      micro_performance_academics_department_id: 1,
      student_id: this.student_id,
      subjects: subjects,
    };

    this.loading = true;
    this.http
      .postApi(this.http.api.storecounsellingaffairs, data, true)
      .then((res: any) => {
        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);
        }
        this.loading = false;
      });
    console.log(this.formData);
  }

  checkmark(x, i, e) {
    this.formData[i][`${e.target.name}`] = e.detail.checked;
  }
  getDetails(id) {
    this.loading = true;
    this.http
      .getApi(this.http.api.getsinglestudent + id, false)
      .then((res: any) => {
        this.studentData = res.data;
        this.formData = this.studentData.subjects;
        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);
        }

        this.getCounsellingAffairs(this.student_id);
      });
  }

  getCounsellingAffairs(id, d = null) {
    console.log({ id });

    var self = this;

    var str = this.http.api.counsellingAffairsByStudent + '/' + id;
    if(d){
      str += '?date='+d;
    }

    this.http
      .getApi(str, false)
      .then((res: any) => {
        if (res.status == true) {
          let data = res.data;
          let details: any[] = data.data;
          console.log({ details });

          if (details.length > 0) {
            let top = details[0];

            this.sendId.emit({id: top.id, flag: 'co'});

            if(top.subjects.length > 0){


              this.formData = top.subjects.map( x => {
                x['student_subject_id'] = x.student_subject_id;

                x.associate_teacher = parseInt(x.associate_teacher, 10);
                x.meet_the_parents = parseInt(x.meet_the_parents, 10);
                x.meet_the_teacher = parseInt(x.meet_the_teacher, 10);
                x.peer_tutoring = parseInt(x.peer_tutoring, 10);
                x.recommendations = parseInt(x.recommendations, 10);
                x.weekly_counselling = parseInt(x.weekly_counselling, 10);

                return x;

              });



            }

          }else{
            this.general.presentToast("No Records Found")
          }

          this.loading = false;
        }
      });
  }

  setFormDate($event){    
    var d = this.date ? this.date.substr(0, this.date.indexOf('T')) : null;    
    this.getCounsellingAffairs(this.student_id, d)
  }

  ngOnInit() {
    // console.log("details project");
    this.route.queryParams.subscribe((params) => {
      this.id = params["id"];
      this.student_id = params["student_id"];
      this.getDetails(this.id);
      this.fname = params["fname"];
      this.lname = params["lname"];
      this.fullname = this.fname + " " + this.lname;


    });

  }
}
