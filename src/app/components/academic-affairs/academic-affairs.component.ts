import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { AlertController } from "@ionic/angular";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { GeneralService } from 'src/app/providers/general.service';
import { GlobaldataService } from 'src/app/providers/globaldata.service';
import { HttpService } from 'src/app/providers/http.service';

@Component({
  selector: 'app-academic-affairs',
  templateUrl: './academic-affairs.component.html',
  styleUrls: ['./academic-affairs.component.scss'],
})

export class AcademicaffairsComponent implements OnInit {
  platform: any = "android";
  fullname: any;
  @Input() inOtherPage: boolean = false;
  @Input() fname: any;
  @Input() lname: any;
  @Input() id: any;
  @Input() student_id: any;
  @Output('sendId') sendId: EventEmitter<any> = new EventEmitter<any>();
  formgroup: FormGroup;
  number: AbstractControl;
  counsellor: AbstractControl;
  reasons: any = [];
  reason_student: any;
  student_remarks: any;
  parent_concern: any;
  studentData: any;
  isSubmitted: boolean = false;
  submitData: any;
  monthIndexes: any;
  date = null;
  monthTest = '01';
  monthGroup = 'Science';
  month: string = "";
  monthList = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];

  formData = [];
  defaultCheckMarks = [
    {
      label: 'Student consistently/often/usually absent.',
      checked: false
    },
    {
      label: 'Student is habitual/often/usually late comer.',
      checked: false
    },
    {
      label: 'Student argues/misbehaves with teacher/students.',
      checked: false
    },
    {
      label: 'Student does not submit homework',
      checked: false
    },
    {
      label: 'Arrival',
      checked: false
    },
    {
      label: 'Student is inattentive/not getting proper sleep/distracted.',
      checked: false
    },
    {
      label: 'Student is unwell/physically weak/malnutritioned.',
      checked: false
    },
    {
      label: 'Student is not responsive/does not participate/shy.',
      checked: false
    },
  ];

  loading = false;


  constructor(
    public alertController: AlertController,
    private http: HttpService,
    private route: ActivatedRoute,
    private general: GeneralService,
  ) {



  }

  submit() {
    // var service_data = {
    //   student_id: this.student_id,
    //   name: this.fullname,
    //   // contact: this.number.value,
    //   // counsellor_alloted: this.counsellor.value,
    //   reason_student: this.reason_student,
    //   student_remarks: this.student_remarks,
    //   parent_concern: this.parent_concern,
    //   reasons: this.reasons,
    // };


    if (this.monthIndexes === '' || this.monthIndexes === undefined) {
      this.general.presentToast('Month must be selected');
      return;
    }

    let subjects = [];
    let error = false;
    this.formData.forEach(element => {

      if (!element.teacher_remarks || element.teacher_remarks == '') {
        this.general.presentToast('Teacher remarks must be filled');
        error = true;
      }

      if (!element.academic_depart_remarks || element.academic_depart_remarks == '') {
        this.general.presentToast('Academic remarks must be filled');
        error = true;
      }

      let obj = {
        student_subject_id: element.id,
        test_percentage: 1,
        teacher_remarks: element.teacher_remarks,
        academics_depart_remarks: element.academic_depart_remarks
      }

      console.log(element.checkmarks);
      for (var j = 0; j < element.checkmarks.length; j++) {
        let n = j + 1
        obj['question_' + n] = element.checkmarks[j].checked
      }

      subjects.push(obj);



    });

    if (error) {
      return
    }
    console.log(subjects);
    let data = {
      student_id: this.studentData.student_id,
      user_id: GlobaldataService.userObject.id,
      monthly_test_no: this.monthTest,
      month: this.monthIndexes,
      year: '2021',
      group: this.monthGroup,
      subjects: subjects
    }



    this.loading = true;
    this.http
      .postApi(this.http.api.storeacademicaffairs, data, false)
      .then((res: any) => {
        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);
        }
        this.loading = false;
      });
    console.log(data)
  }

  getDetails(id) {

    this.loading = true;
    this.http
      .getApi(this.http.api.getsinglestudent + id, false)
      .then((res: any) => {
        console.log(res)
        this.studentData = res.data;
        this.formData = this.studentData.subjects;
        for (var i = 0; i < this.formData.length; i++) {

          this.formData[i].checkmarks = [];
          for (var j = 0; j < this.defaultCheckMarks.length; j++) {
            this.formData[i].checkmarks.push({
              id: i + j,
              label: this.defaultCheckMarks[j].label,
              checked: this.defaultCheckMarks[j].checked
            })
          }

        }

        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);
        }

        this.getAcademicAffairs(this.student_id);
      });
  }

  ngOnInit() {
    console.log("details project");
    this.route.queryParams.subscribe((params) => {
      this.id = params["id"];
      this.student_id = params["student_id"];
      this.getDetails(this.id);
      this.fname = params["fname"];
      this.lname = params["lname"];
      this.fullname = this.fname + " " + this.lname;


    });

  }

  selectMonth(e, i) {
    console.log(e)
    this.monthIndexes = e.detail.value;
  }

  setFormDate($event){    
    var d = this.date ? this.date.substr(0, this.date.indexOf('T')) : null;    
    this.getAcademicAffairs(this.student_id, d)
  }

  getAcademicAffairs(id, d = null) {
    console.log({ id });

    var str = this.http.api.academicAffairsByStudentId + '/' + id;
    if(d){
      str += '?date='+d;
    }

    this.http.getApi(str, false).then((res: any) => {

      if (res.status == true) {
        let data = res.data;
        let details: any[] = data.data;

        if (details.length > 0) {
          let top = details[0];

          this.sendId.emit({id: top.id, flag: 'ac'});

          this.monthTest = top.monthly_test_no;
          this.monthGroup = top.group;
          this.month = top.month;
          // let forms = top.subjects as [];

          if (top.subjects.length > 0) {



            for (var s = 0; s < top.subjects.length; s++) {

              var temp = [];
              for (var i = 0; i < this.defaultCheckMarks.length; i++) {


                let n = i + 1;
                if (top.subjects[s].hasOwnProperty('question_' + n)) {
                  let h = top.subjects[s]['question_' + n] == "1";
                  let d = Object.assign({}, this.defaultCheckMarks[i]);
                  d.checked = h;
                  d['id'] = s + i + n;
                  console.log({ h });
                  temp[i] = d;

                }

              }

              top.subjects[s].checkmarks = temp;

              top.subjects[s].academic_depart_remarks = top.subjects[s].academics_depart_remarks;

            }

            console.log(top.subjects);


          }



          this.formData = top.subjects;


        }else{
          this.general.presentToast("No Records Found")
        }

        this.loading = false;




      }

    });
  }
}



