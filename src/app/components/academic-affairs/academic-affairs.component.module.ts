import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AcademicaffairsComponent } from './academic-affairs.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,

  ],
  declarations: [AcademicaffairsComponent],
  exports: [AcademicaffairsComponent]
})
export class AcademicAffairsComponentModule { }
