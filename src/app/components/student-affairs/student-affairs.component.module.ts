import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";
import { StudentAffairsComponent } from "./student-affairs.component";








@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,

  ],
  declarations: [StudentAffairsComponent],
  exports: [StudentAffairsComponent]
})
export class StudentaffairsComponentModule { }
