import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { GeneralService } from 'src/app/providers/general.service';
import { HttpService } from 'src/app/providers/http.service';

@Component({
  selector: 'app-student-affairs',
  templateUrl: './student-affairs.component.html',
  styleUrls: ['./student-affairs.component.scss'],
})
export class StudentAffairsComponent implements OnInit {

  platform: any = "android";
  fullname: any;
  @Input() inOtherPage: boolean = false;
  @Input() fname: any;
  @Input() lname: any;
  @Input() id: any;
  @Input() student_id: any;
  @Output('sendId') sendId: EventEmitter<any> = new EventEmitter<any>();
  formgroup: FormGroup;
  number: AbstractControl;
  counsellor: AbstractControl;
  reasons: any = [
    {
      label: "Without Card",
      checked: false
    },
    {
      label: "Absenteesim",
      checked: false
    },
    {
      label: "Improper Uniform",
      checked: false
    },
    {
      label: "Teacher Complain",
      checked: false
    },
    {
      label: "Arrival",
      checked: false
    },
    {
      label: "Discipline Issues",
      checked: false
    },
    {
      label: "Bunked",
      checked: false
    },
    {
      label: "Excused for leave",
      checked: false
    },
    {
      label: "Early Leave",
      checked: false
    },
    {
      label: "Others",
      checked: false
    }

  ];
  reason_student: any;
  student_remarks: any;
  parent_concern: any;
  date = null;
  isSubmitted: boolean = false;
  loading = false;

  constructor(
    public alertController: AlertController,
    private http: HttpService,
    public formbuilder: FormBuilder,
    private route: ActivatedRoute,
    private general: GeneralService,
  ) {
    this.formgroup = formbuilder.group({
      number: ["", [Validators.required]],
      counsellor: ["", [Validators.required]],
    });
    this.number = this.formgroup.controls["number"];
    this.counsellor = this.formgroup.controls["counsellor"];
  }

  setFormDate($event){    
    var d = this.date ? this.date.substr(0, this.date.indexOf('T')) : null;    
    this.getStudentAffairs(this.student_id, d)
  }


  submit() {

    console.log(this.reasons)
    var indices = [];
    for (var i = 0; i < this.reasons.length; i++) {
      if (this.reasons[i].checked == true) {
        indices.push(i + 1)
      }
    }

    var service_data = {
      student_id: this.student_id,
      name: this.fullname,
      contact: this.number.value,
      counsellor_alloted: this.counsellor.value,
      reason_student: this.reason_student,
      student_remarks: this.student_remarks,
      parent_concern: this.parent_concern,
      reasons: indices,
    };

    this.loading = true;
    this.http
      .postApi(this.http.api.storestudentaffair, service_data, true)
      .then((res: any) => {
        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);
        }
        this.loading = false;
      });
  }



  ngOnInit() {
    console.log("details project");
    this.route.queryParams.subscribe((params) => {
      this.id = params["id"];
      this.student_id = params["student_id"];
      this.getStudentAffairs(this.student_id)
      this.fname = params["fname"];
      this.lname = params["lname"];
      this.fullname = this.fname + " " + this.lname;


    });
    // this.getdetails();
  }

  getStudentAffairs(id, d = null) {
    console.log({ id });
    this.loading = true;

    var str = this.http.api.studentAffairsByStudentId + '/' + id;
    if(d){
      str += '?date='+d;
    }

    console.log(str);

    this.http.getApi( str , false).then((res: any) => {
      console.log({ res });

      if (res.status == true) {
        let data = res.data;

        let details: any[] = data.data;

        if (details.length > 0) {
          let data = details[0];
          console.log({ data });
          this.sendId.emit({id: data.id, flag: 'sa'});
          this.student_id = data['student_id'];
          this.fullname = data['name'];
          this.reason_student = data['reason_student'];
          this.student_remarks = data['student_remarks'];
          this.parent_concern = data['parent_concern'];
          let reasons: any[] = data['reasons'];
          reasons = reasons.map((x) => parseInt(x.affair_reason_id))
          let nums = [...new Set(reasons)];

          this.reasons.map(x => {
            x.checked = false;
            return x
          });

          for (var i = 0; i < nums.length; i++) {
            let n = nums[i] - 1;
            if (n >= 0 && n <= 9) {
              this.reasons[n].checked = true
            }

          }


          this.formgroup.setValue({
            number: data['contact'],
            counsellor: data['counsellor_alloted'],
          });
          // this.number = data['contact'];
          // this.counsellor = data['counsellor_alloted'];

        }else{
          this.general.presentToast("No Records Found")
        }

        this.loading = false;

      } else {

      }
    });
  }


}
