import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CouncelingPage } from './counceling.page';

const routes: Routes = [
  {
    path: '',
    component: CouncelingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CouncelingPageRoutingModule {}
