import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { HttpService } from 'src/app/providers/http.service';

@Component({
  selector: 'app-counceling',
  templateUrl: './counceling.page.html',
  styleUrls: ['./counceling.page.scss'],
})
export class CouncelingPage implements OnInit {

  list: any = [];
  loading = false;
  student;

  constructor(private location: Location, private http: HttpService) { }

  goback() {
    this.location.back();
  }

  ngOnInit() {

    let st = localStorage.getItem('selectedStudent');
    if (st) {
      this.student = JSON.parse(st)
      console.log("got student", { ...this.student })
      this.getCounsellingAffairs();
    }

  }

  getCounsellingAffairs() {

    this.http
      .getApi(this.http.api.counsellingAffairsByStudent + '/' + this.student.student_id, false)
      .then((res: any) => {
        if (res.status == true) {
          let data = res.data;

          let details: any[] = data.data;
          console.log("details", details);


          if (details.length > 0) {
            // let data = details[0];
            this.list = details;
            console.log("data", data);
          }
        }
      });
  }

}
