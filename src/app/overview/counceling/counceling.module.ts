import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CouncelingPageRoutingModule } from './counceling-routing.module';

import { CouncelingPage } from './counceling.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CouncelingPageRoutingModule
  ],
  declarations: [CouncelingPage]
})
export class CouncelingPageModule {}
