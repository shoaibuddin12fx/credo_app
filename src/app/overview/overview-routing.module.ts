import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewPage } from './overview.page';

const routes: Routes = [
  {
    path: '',
    component: OverviewPage,
    children: [
      {
        path: "",
        redirectTo: "affairs",
        pathMatch: "full",
      },
      {
        path: 'affairs',
        loadChildren: () => import('./affairs/affairs.module').then(m => m.AffairsPageModule)
      },
      {
        path: 'academic',
        loadChildren: () => import('./academic/academic.module').then(m => m.AcademicPageModule)
      },
      {
        path: 'counceling',
        loadChildren: () => import('./counceling/counceling.module').then(m => m.CouncelingPageModule)
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OverviewPageRoutingModule { }
