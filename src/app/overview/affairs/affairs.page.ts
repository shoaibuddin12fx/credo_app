import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { HttpService } from 'src/app/providers/http.service';

@Component({
  selector: 'app-affairs',
  templateUrl: './affairs.page.html',
  styleUrls: ['./affairs.page.scss'],
})
export class AffairsPage implements OnInit {


  list: any = [];
  loading = false;
  student;

  constructor(private location: Location, private http: HttpService) { }

  goback() {
    this.location.back();
  }

  ngOnInit() {

    let st = localStorage.getItem('selectedStudent');
    if (st) {
      this.student = JSON.parse(st)
      console.log("got student", { ...this.student })
      this.getStudentAffairs();
    }

  }

  getStudentAffairs() {
    // console.log({ this.id });
    this.loading = true;
    console.log("Std_id", this.student.id);

    this.http.getApi(this.http.api.studentAffairsByStudentId + '/' + this.student.student_id, false).then((res: any) => {
      console.log({ res });
      if (res.status == true) {
        let data = res.data;

        let details: any[] = data.data;
        console.log("details", details);


        if (details.length > 0) {
          // let data = details[0];
          this.list = details;
          console.log("data", data);
        }
      }
    });
  }

}
