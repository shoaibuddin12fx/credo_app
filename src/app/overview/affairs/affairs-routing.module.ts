import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AffairsPage } from './affairs.page';

const routes: Routes = [
  {
    path: '',
    component: AffairsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AffairsPageRoutingModule {}
