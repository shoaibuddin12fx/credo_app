import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AffairsPageRoutingModule } from './affairs-routing.module';

import { AffairsPage } from './affairs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AffairsPageRoutingModule
  ],
  declarations: [AffairsPage]
})
export class AffairsPageModule {}
