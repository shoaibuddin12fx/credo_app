import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { HttpService } from '../providers/http.service';
@Component({
  selector: 'app-overview',
  templateUrl: './overview.page.html',
  styleUrls: ['./overview.page.scss'],
})
export class OverviewPage implements OnInit {

  constructor(private location: Location, private http: HttpService) {

  }
  goback() {
    this.location.back();
  }

  ngOnInit() {

  }




}
