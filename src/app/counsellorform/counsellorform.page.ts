import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ModalService } from '../service/modal.service';
import { HttpService } from '../providers/http.service';
import { GeneralService } from '../providers/general.service';
@Component({
  selector: 'app-counsellorform',
  templateUrl: './counsellorform.page.html',
  styleUrls: ['./counsellorform.page.scss'],
})
export class CounsellorformPage implements OnInit {

  councellorData = {
    name: "",
    email: "",
    phone: ""
  }

  constructor(
    private location: Location,
    private modals: ModalService,
    private http: HttpService,
    private general: GeneralService,
  ) { }

  ngOnInit() {
  }
  goback() {
    // this.location.back();
    this.modals.dismiss({data: 'A'})
  }

  submit() {
    console.log(this.councellorData);

    this.http
        .postApi2(this.http.api.addcounsellor, this.councellorData, true)
        .then((res: any) => {
          console.log(res);
          if (res.status == true) {

            this.modals.dismiss(res.data);
            
          } else {
            this.general.presentToast(res.message);
          }
        });

  }

}
