import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CounsellorformPageRoutingModule } from './counsellorform-routing.module';

import { CounsellorformPage } from './counsellorform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CounsellorformPageRoutingModule
  ],
  declarations: [CounsellorformPage]
})
export class CounsellorformPageModule {}
