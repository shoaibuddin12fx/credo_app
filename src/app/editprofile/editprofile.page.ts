import { Component, OnInit } from "@angular/core";
import { GlobaldataService } from "../providers/globaldata.service";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpService } from "../providers/http.service";
import { AlertController } from "@ionic/angular";
import { GeneralService } from "../providers/general.service";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Storage } from "@ionic/storage";
import { Events } from "../events";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject,
} from "@ionic-native/file-transfer/ngx";

import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-editprofile",
  templateUrl: "./editprofile.page.html",
  styleUrls: ["./editprofile.page.scss"],
})
export class EditprofilePage implements OnInit {
  formgroup: FormGroup;
  email: AbstractControl;
  phone: AbstractControl;
  image: any;
  name: AbstractControl;
  namevalue: any;
  phonevalue: any;
  emailvalue: any;
  uploadedimageurl: any;
  isSubmitted: boolean = false;
  timeStampDate: any;
  platform: any;
  constructor(
    private transfer: FileTransfer,
    private camera: Camera,
    private storage: Storage,
    public events: Events,
    private general: GeneralService,
    private http: HttpService,
    public alertController: AlertController,
    public formbuilder: FormBuilder,
    private router: Router
  ) {
    let NAMEPATTERN = /^[^-\s][a-zA-Z_\s-]+$/i;
    let EMAILPATTERN = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/i;
    this.formgroup = formbuilder.group({
      name: ["", [Validators.required, Validators.pattern(NAMEPATTERN)]],
      email: ["", [Validators.required, Validators.pattern(EMAILPATTERN)]],
      phone: ["", [Validators.required]],
    });
    this.email = this.formgroup.controls["email"];
    this.name = this.formgroup.controls["name"];
    this.phone = this.formgroup.controls["phone"];

    this.emailvalue = GlobaldataService.userObject.email;
    this.namevalue = GlobaldataService.userObject.name;
    this.phonevalue = GlobaldataService.userObject.phone;
    this.image = GlobaldataService.userObject.defaultavatar;
  }

  ngOnInit() {
    this.platform = GlobaldataService.platform;
    console.log("edit page");
  }

  uploadPhoto(path: string) {
    this.general.presentLoading();
    let random4DigitValue = Math.floor(1000 + Math.random() * 9000);
    this.timeStampDate = Date.now();

    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: "avatar",
      fileName:
        "profilepic" + this.timeStampDate + "_" + random4DigitValue + ".jpg",
      chunkedMode: false,
      headers: { Authorization: "Bearer " + GlobaldataService.token },
      //mimeType: "image/jpeg",
    };
    fileTransfer
      .upload(path, "https://marketplace.ycsols.com/api/uploadavatar", options)
      .then(
        (data) => {
          this.general.stopLoading();
          console.log(data + " Uploaded Successfully");
          console.log(JSON.parse(data.response));
          let res = JSON.parse(data.response);
          // this.general.presentToast('Media Uploaded');
          if (res.success == true) {
            this.image = res.data.defaultavatar;
            GlobaldataService.userObject = res.data;
            this.storage.set("userObject", res.data);
            this.general.presentToast(res.message);
            this.events.publish("user:created", {
              name: res.data.name,
              email: res.data.email,
              image: res.data.defaultavatar,
            });
            this.general.presentToast(res.message);
            GlobaldataService.netcheck = true;
            // this.getprofile();
            // this.confirmImage = res.image_name;
            // fileType == ".png" ? (this.is_image = 1) : (this.is_image = 0);
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  async select() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Upload Picture",
      message: "Please choose from one of the below",
      buttons: [
        {
          text: "Camera",
          cssClass: "secondary",
          handler: (blah) => {
            this.takePhoto();
          },
        },
        {
          text: "Gallery",
          handler: () => {
            this.chosePhoto();
          },
        },
      ],
    });

    await alert.present();
  }

  updateinfo() {
    this.isSubmitted = true;
    console.log(this.formgroup.value);
    if (this.formgroup.valid) {
      var service_data = {
        name: this.name.value,
        phonenumber: this.phone.value,
      };
      this.http
        .postApi(this.http.api.editprofile, service_data, true)
        .then((res: any) => {
          console.log(res);
          if (res.success == true) {
            GlobaldataService.userObject = res.data;
            this.storage.set("userObject", res.data);
            this.general.presentToast(res.message);
            this.events.publish("user:created", {
              name: res.data.name,
              email: res.data.email,
              image: res.data.defaultavatar,
            });
            // this.router.navigate(["tabs/projects"]);
          } else {
            this.general.presentToast(res.message);
          }
        });
    }
  }

  takePhoto() {
    GlobaldataService.netcheck = false;
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true,
    };

    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        // this.image = imageData;
        this.uploadPhoto(imageData);
      },
      (err) => {
        console.log(err);
        // Handle error
      }
    );
  }

  chosePhoto() {
    GlobaldataService.netcheck = false;
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true,
    };

    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        // this.image = imageData;
        this.uploadPhoto(imageData);
      },
      (err) => {
        console.log(err);
        // Handle error
      }
    );
  }

  changepassword() {
    this.router.navigate(["changepassword"]);
  }
}
