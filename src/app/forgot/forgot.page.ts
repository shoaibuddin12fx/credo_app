import { Component, OnInit } from "@angular/core";
import { HttpService } from "../providers/http.service";
import { GeneralService } from "../providers/general.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NavigationExtras } from "@angular/router";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-forogt",
  templateUrl: "./forgot.page.html",
  styleUrls: ["./forgot.page.scss"],
})
export class ForgotPage implements OnInit {
  public logoAnimation: string = "";
  public restAnimation: string = "";
  formgroup: FormGroup;
  email: AbstractControl;
  isSubmitted: boolean = false;
  constructor(
    private http: HttpService,
    private general: GeneralService,
    public formbuilder: FormBuilder,
    private router: Router
  ) {
    GlobaldataService.currentPage = "forgotpassword";
    let EMAILPATTERN = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/i;
    this.formgroup = formbuilder.group({
      email: ["", [Validators.required, Validators.pattern(EMAILPATTERN)]],
    });
    this.email = this.formgroup.controls["email"];
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.formgroup.valid) {
      console.log("Please provide all the required values!");
      return false;
    } else {
      console.log(this.formgroup.value);
      var service_data = {
        email: this.email.value,
      };
      this.http
        .postApi2(this.http.api.forgotpassword, service_data, true)
        .then((res: any) => {
          console.log(res);
          if (res.success == true) {
            this.router.navigate(["login"]);
            this.general.presentToast(res.message);
          } else {
            this.general.presentToast(res.message);
          }
        });
    }
  }

  gotologin() {
    this.router.navigate(["login"]);
  }

  about(pagename) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        name: pagename,
      },
    };
    this.router.navigate(["about"], navigationExtras);
  }

  applyAnimation() {
    this.logoAnimation = "animated " + "fadeInDown";
    this.restAnimation = "animated " + "fadeInUp";
  }

  ionViewWillEnter() {
    this.applyAnimation();
  }

  ngOnInit() {
    console.log("forgot password");
  }
}
