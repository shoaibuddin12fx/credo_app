import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full",
  },
  {
    path: "about",
    loadChildren: () =>
      import("./about/about.module").then((m) => m.AboutPageModule),
  },

  {
    path: "projects",
    loadChildren: () =>
      import("./projects/projects.module").then((m) => m.ProjectsPageModule),
  },

  {
    path: "nointernet",
    loadChildren: () =>
      import("./nointernet/nointernet.module").then(
        (m) => m.NointernetPageModule
      ),
  },

  {
    path: "editprofile",
    loadChildren: () =>
      import("./editprofile/editprofile.module").then(
        (m) => m.EditprofilePageModule
      ),
  },
  {
    path: "changepassword",
    loadChildren: () =>
      import("./changepassword/changepassword.module").then(
        (m) => m.ChangepasswordPageModule
      ),
  },

  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginPageModule),
  },
  {
    path: "register",
    loadChildren: () =>
      import("./register/register.module").then((m) => m.RegisterPageModule),
  },
  {
    path: "forgot",
    loadChildren: () =>
      import("./forgot/forgot.module").then((m) => m.ForgotPageModule),
  },

  {
    path: "studentaffairs",
    loadChildren: () =>
      import("./studentaffairs/studentaffairs.module").then(
        (m) => m.StudentaffairsPageModule
      ),
  },
  {
    path: 'academicaffairs',
    loadChildren: () => import('./academicaffairs/academicaffairs.module').then(m => m.AcademicaffairsPageModule)
  },
  {
    path: 'counsellingaffairs',
    loadChildren: () => import('./counsellingaffairs/counsellingaffairs.module').then(m => m.CounsellingaffairsPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
  },
  {
    path: 'selectCounsellor',
    loadChildren: () => import('./select-counsellor/select-counsellor.module').then(m => m.SelectCounsellorPageModule)
  },
  {
    path: 'popover',
    loadChildren: () => import('./popover/popover.module').then(m => m.PopoverPageModule)
  },
  {
    path: 'overview',
    loadChildren: () => import('./overview/overview.module').then(m => m.OverviewPageModule)
  },  {
    path: 'counsellorform',
    loadChildren: () => import('./counsellorform/counsellorform.module').then( m => m.CounsellorformPageModule)
  },





];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
