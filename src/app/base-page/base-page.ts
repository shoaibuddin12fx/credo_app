import { Injector } from "@angular/core";
import { NavService } from "../service/nav.service";


export abstract class BasePage {
  public nav: NavService;

  constructor(injector: Injector) {
    this.nav = injector.get(NavService);
  }
}
