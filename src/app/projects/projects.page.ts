import { Component, Injector, OnInit } from "@angular/core";
import { MenuController, ModalController, PopoverController } from "@ionic/angular";
import { HttpService } from "../providers/http.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { GeneralService } from "../providers/general.service";
import { ActivatedRouteSnapshot, Router } from "@angular/router";
import { NavigationExtras } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { Location } from "@angular/common";
import { PopoverPage } from "../popover/popover.page";
import { BasePage } from "../base-page/base-page";
import { Storage } from "@ionic/storage";



@Component({
  selector: "app-projects",
  templateUrl: "./projects.page.html",
  styleUrls: ["./projects.page.scss"],
})
export class ProjectsPage extends BasePage implements OnInit {
  students: any = [];
  next: any;
  particularStudent;
  platform: any;
  public items: any = [];
  temp_items: any = [];
  lastBack: any;
  currentSelected: any = 0;
  categories: any;
  catname: any;
  loading = false;
  public searchTerm: string = "";
  isEnablePopover = false;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    private router: Router,
    public menuCtrl: MenuController,
    private location: Location,
    // private activatedRouteSnapshot: ActivatedRouteSnapshot,
    private http: HttpService,
    public popoverController: PopoverController,
    public storage: Storage,
    injector: Injector
  ) {
    super(injector);
    document.addEventListener(
      "backbutton",
      () => {
        if (window.location.pathname == "/projects") {
          if (Date.now() - this.lastBack < 500) {
            // logic for double tap: delay of 500ms between two clicks of back button
            navigator["app"].exitApp();
          }
          this.lastBack = Date.now();
        }

        if (
          window.location.pathname != "/projects" &&
          window.location.pathname != "login"
        ) {
          this.goback();
        }
        if (window.location.pathname == "/login") {
          if (Date.now() - this.lastBack < 500) {
            // logic for double tap: delay of 500ms between two clicks of back button
            navigator["app"].exitApp();
          }
          this.lastBack = Date.now();
        }
      },
      false
    );
  }

  goback() {
    this.location.back();
  }

  async getstudents() {

    this.loading = true;

    console.log("global", GlobaldataService.userObject)

    if (GlobaldataService.userObject.department_name == "Counselling" || GlobaldataService.userObject.department_name == "Admin" || GlobaldataService.userObject.department_name == "Management") {
      this.isEnablePopover = true;
    } else {
      this.isEnablePopover = false;
    }

    const user = GlobaldataService.userObject; // await this.storage.get("userObject");
    console.log(user);

    const string = this.http.api.getstudents + "?counsellor_id=" + user.id + "&role=" + user.role;

    this.http.getApi(string, false).then((res: any) => {

      if (res.status == true) {
        this.students = res.data.data;
        this.items = res.data.data;
        this.temp_items = this.items;
        this.next = res.data.next_page_url;
      }
      this.loading = false;
      
    });
  }

  async comingsoon() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Coming Soon",
      message: "Get Excited!",
      buttons: [
        {
          text: "Okay",
          handler: () => { },
        },
      ],
    });

    await alert.present();
  }

  getmore(event) {


    this.http.pagination(this.next, false).then((res: any) => {
      console.log(res);
      if (res.status == true) {
        for (var v in res.data.data) {
          this.students.push(res.data.data[v]);
          this.items.push(res.data.data[v]);
        }
        this.next = res.data.next_page_url;
        event.target.complete();
      }


    });
  }

  doInfinite(event) {
    if (this.next == null) {
      event.target.complete();
    }

    if (this.next != null) {
      this.getmore(event);
    }
  }

  gotoprojectdetails(id, studentid, fname, lname) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: id,
        fname: fname,
        lname: lname,
        student_id: studentid,
      },
    };
    //this.router.navigate(["counsellingaffairs"], navigationExtras);

    // GlobaldataService.userObject.department_name = "Academics";

    console.log(GlobaldataService.userObject);

    if (GlobaldataService.userObject.department_name == "Student Affairs") {
      this.router.navigate(["studentaffairs"], navigationExtras);
    }
    if (GlobaldataService.userObject.department_name == "Academics") {
      this.router.navigate(["academicaffairs"], navigationExtras);
    }
    if (GlobaldataService.userObject.department_name == "Counselling" || GlobaldataService.userObject.department_name == "Admin" || GlobaldataService.userObject.department_name == "Management") {
      this.router.navigate(["counsellingaffairs"], navigationExtras);
    }
  }

  gotopackages() {
    this.router.navigate(["packages"]);
  }

  filterItems(searchTerm) {
    return this.items.filter((item) => {
      return (
        item.first_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      );
    });
  }

  setFilteredItems() {
    if (this.searchTerm == "") {
      this.items = this.temp_items;
    } else {
      this.items = this.filterItems(this.searchTerm);
    }
  }

  ionViwWillEnter() { }

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.platform = GlobaldataService.platform;
    // this.getprojects();

  }

  ionViewWillEnter() {
    this.getstudents();
  }

  async popclick(event, item) {
    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     id: ''
    //   }
    // }

    const popover = await this.popoverController.create({
      component: PopoverPage,
      event
    });

    popover.onDidDismiss().then((data) => {
      console.log(data)

      if (data.data != '') {
        let _data = data.data;
        if (_data.key == 'overview') {

          localStorage.setItem('selectedStudent', JSON.stringify(item))
          this.router.navigateByUrl('overview');
        }

        if (_data.key == 'profile') {

          localStorage.setItem('selectedStudent', JSON.stringify(item))
          this.router.navigateByUrl('profile');
        }

        if (_data.key == 'selectCounsellor') {

          localStorage.setItem('selectedStudent', JSON.stringify(item))
          this.router.navigateByUrl('selectCounsellor');
        }

      }
      console.log('onDidDismiss resolved with role', data);
    });


    return await popover.present();



  }

  addStudentAffair() {
    this.nav.push("studentaffairs")
  }

}
