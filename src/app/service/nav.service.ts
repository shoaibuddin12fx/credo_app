import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class NavService {
  dismiss(arg0: string) {
    throw new Error('Method not implemented.');
  }

  //
  constructor(
    public location: Location,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) { }

  serialize = (obj) => {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  };

  async setRoot(page, param = {}) {
    // await this.nativePageTransitions.fade(null);
    const extras: NavigationExtras = {
      queryParams: param,
    };
    this.navigateTo(page, extras);
  }

  async push(page, param = {}) {
    const extras: NavigationExtras = {
      queryParams: param,
    };
    console.log('got url', { page });
    this.navigateTo(page, extras);
  }

  async pop(param = {}) {
    return new Promise<void>(async (resolve) => {
      const str = this.serialize(param);
      this.location.back() + '?' + str;
      // this.router.navigate([".."], param);
      resolve();
    });
  }

  navigateTo(link, data?: NavigationExtras) {
    this.router.navigate([link], data);
  }

  navigateToChild(link, data?: NavigationExtras) {
    data.relativeTo = this.activatedRoute;
    this.router.navigate([link], data);
  }

  getParams() {
    return this.activatedRoute.snapshot.params;
  }

  getQueryParams() {
    return this.activatedRoute.snapshot.queryParams;
  }
}
