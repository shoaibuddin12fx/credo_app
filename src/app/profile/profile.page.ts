import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Location } from "@angular/common";
import { HttpService } from 'src/app/providers/http.service';
import { GeneralService } from '../providers/general.service';
import { ImageService } from '../service/image.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  student;
  loading = false;
  userImage = "assets/images/male.jpg";

  // @Input('image') image = "";

  @Output('setImage') setImage: EventEmitter<any> = new EventEmitter<any>()

  img = '';

  timestamp = Date.now();



  /*
class_id: "1"
created_at: "2021-09-11T19:28:15.000000Z"
father_email: null
father_name: null
father_phone: null
first_name: "Zainab"
id: 792
image: null
last_name: "Shaheen"
phone_number: null
section_id: "5"
student_id: "2019213"
updated_at: "2021-09-11T19:28:15.000000Z"
  */

  constructor(
    private location: Location,
    private http: HttpService,
    private general: GeneralService,
    public imageService: ImageService
  ) {

  }

  goback() {
    this.location.back();
  }

  ionViewWillEnter() {
    let st = localStorage.getItem('selectedStudent');
    if (st) {
      this.student = JSON.parse(st)
      console.log("got student", { ...this.student })
      this.getStudentProfile();
    }
  }
  ngOnInit() {

  }

  getStudentProfile() {

    this.http.getApi(this.http.api.getstudentProfile + '/' + this.student.id, false).then((res: any) => {
      console.log({ res });
      this.student = res.data;
      if (res.data.image)
        this.userImage = this.http.BASE_PATH + '/' + res.data.image;
    });

  }

  setStudentProfile() {
    this.loading = true;
    this.http
      .postApi(this.http.api.setstudentProfile + '/' + this.student.id, this.student, true)
      .then((res: any) => {
        console.log(res);
        if (res.success == true) {
          this.general.presentToast(res.message);
        }
        this.loading = false;
      });

  }
  // async getimage() {

  //   this.img = await this.imageService.getSnap() as string;
  //   this.image = this.img;
  //   this.timestamp = Date.now();
  //   this.setImage.emit(this.image);
  // }
  async captureImage() {
    let res = await this.imageService.getSnap();
    console.log(res);
    this.userImage = res.toString();
    this.http
      .postApi(this.http.api.setpicture, { student_id: this.student.id, image: this.userImage }, false)
      .then((res: any) => {
        console.log(res);
      })
  }

}
