import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcademicaffairsPageRoutingModule } from './academicaffairs-routing.module';

import { AcademicaffairsPage } from './academicaffairs.page';
import { AcademicAffairsComponentModule } from '../components/academic-affairs/academic-affairs.component.module';
import { StudentaffairsComponentModule } from '../components/student-affairs/student-affairs.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcademicaffairsPageRoutingModule,
    StudentaffairsComponentModule,
    AcademicAffairsComponentModule
  ],
  declarations: [AcademicaffairsPage]
})
export class AcademicaffairsPageModule { }
