import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AcademicaffairsPage } from './academicaffairs.page';

describe('AcademicaffairsPage', () => {
  let component: AcademicaffairsPage;
  let fixture: ComponentFixture<AcademicaffairsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AcademicaffairsPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AcademicaffairsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
