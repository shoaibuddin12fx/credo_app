import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { HttpService } from "../providers/http.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { GeneralService } from "../providers/general.service";
import { AlertController } from "@ionic/angular";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-academicaffairs",
  templateUrl: "./academicaffairs.page.html",
  styleUrls: ["./academicaffairs.page.scss"],
})
export class AcademicaffairsPage implements OnInit {

  toggle2 = true;
  isAcademic = false;
  constructor(
    private locationapp: Location
  ) {

    this.isAcademic = GlobaldataService.userObject.department_name == "Academics";
  }

  ngOnInit(): void {

  }

  goback() {
    this.locationapp.back();
  }

}
