import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
// import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { IonicStorageModule } from "@ionic/storage";
import { Network } from "@ionic-native/network/ngx";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { FCM } from "@ionic-native/fcm/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { FileTransfer } from "@ionic-native/file-transfer/ngx";

// import {
//   FileTransfer,
//   FileUploadOptions,
//   FileTransferObject,
// } from "@ionic-native/file-transfer/ngx";

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    StatusBar,
    Network,
    Camera,
    FCM,
    FileTransfer,
    AndroidPermissions,
    SplashScreen,

    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
