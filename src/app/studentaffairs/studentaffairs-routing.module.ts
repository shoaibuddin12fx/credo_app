import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentaffairsPage } from './studentaffairs.page';

const routes: Routes = [
  {
    path: '',
    component: StudentaffairsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentaffairsPageRoutingModule {}
