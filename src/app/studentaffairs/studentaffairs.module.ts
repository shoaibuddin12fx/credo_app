import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { StudentaffairsPageRoutingModule } from "./studentaffairs-routing.module";

import { StudentaffairsPage } from "./studentaffairs.page";
import { StudentaffairsComponentModule } from "../components/student-affairs/student-affairs.component.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    StudentaffairsPageRoutingModule,
    StudentaffairsComponentModule
  ],
  declarations: [StudentaffairsPage],
})
export class StudentaffairsPageModule { }
