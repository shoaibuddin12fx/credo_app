import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { HttpService } from "../providers/http.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { GeneralService } from "../providers/general.service";
import { AlertController } from "@ionic/angular";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-studentaffairs",
  templateUrl: "./studentaffairs.page.html",
  styleUrls: ["./studentaffairs.page.scss"],
})
export class StudentaffairsPage implements OnInit {

  toggle = true;
  constructor(

    private locationapp: Location
  ) {

  }
  ngOnInit(): void {

  }

  goback() {
    this.locationapp.back();
  }

}
