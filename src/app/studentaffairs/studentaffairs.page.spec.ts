import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StudentaffairsPage } from './studentaffairs.page';

describe('StudentaffairsPage', () => {
  let component: StudentaffairsPage;
  let fixture: ComponentFixture<StudentaffairsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentaffairsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StudentaffairsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
