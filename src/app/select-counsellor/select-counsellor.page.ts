import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { HttpService } from '../providers/http.service';
import { AlertController, IonButtons } from '@ionic/angular';
import { GeneralService } from '../providers/general.service';
import { NavigationExtras } from '@angular/router';
import { NavService } from '../service/nav.service';
import { ModalService } from '../service/modal.service';
import { CounsellorformPage } from '../counsellorform/counsellorform.page';

@Component({
  selector: 'app-select-counsellor',
  templateUrl: './select-counsellor.page.html',
  styleUrls: ['./select-counsellor.page.scss'],
})
export class SelectCounsellorPage implements OnInit {
  [x: string]: any;

  student: any;
  list = [];
  loading = true;

  constructor(
    private location: Location,
    private http: HttpService,
    private general: GeneralService,
    private nav: NavService,
    private modals: ModalService

  ) { }

  goback() {
    this.location.back();
  }
  ngOnInit() {
    let st = localStorage.getItem('selectedStudent');
    if (st) {
      this.student = JSON.parse(st)
      console.log("got student", { ...this.student })
      this.student.counsellor_id = parseInt(this.student.counsellor_id);
      this.getCounsellors();
    }
  }

  selectCounselor($event) {
    console.log(this.student.counsellor_id);
  }

  save() {
    console.log(this.student.counsellor_id);
    this.http
      .postApi(this.http.api.setCounsellor + '/' + this.student.id, this.student, true)
      .then((res: any) => {
        console.log(res);
        if (res.status == true) {
          this.general.presentToast(res.message);
        }
        this.loading = false;
      });
  }

  getCounsellors() {
    // console.log({ this.id });
    this.loading = true;
    console.log("Std_id", this.student.id);

    this.http.getApi(this.http.api.getCounsellors + '/' + this.student.student_id, false).then((res: any) => {
      console.log({ res });
      if (res.status == true) {
        let data = res.data;

        let details: any[] = data.data;
        console.log("details", details);


        if (details.length > 0) {
          // let data = details[0];
          this.list = details;
          console.log("data", data);
        }
      }
    });
  }

  async councellorForm() {
    console.log("hello");

    const res = await this.modals.present(CounsellorformPage);
    this.getCounsellors();

    // this.nav.push("counsellorform");
  }
}
