import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectCounsellorPageRoutingModule } from './select-counsellor-routing.module';

import { SelectCounsellorPage } from './select-counsellor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectCounsellorPageRoutingModule
  ],
  declarations: [SelectCounsellorPage]
})
export class SelectCounsellorPageModule {}
