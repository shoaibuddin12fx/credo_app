import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpService } from "../providers/http.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { GeneralService } from "../providers/general.service";
import { Location } from "@angular/common";

@Component({
  selector: "app-about",
  templateUrl: "./about.page.html",
  styleUrls: ["./about.page.scss"],
})
export class AboutPage implements OnInit {
  name: any;
  text: any;
  title: any;
  constructor(
    private route: ActivatedRoute,
    private http: HttpService,
    private location: Location
  ) {
    GlobaldataService.currentPage = "about";
  }

  getprivacy() {
    this.http.getApi(this.http.api.privacypolicy, false).then((res: any) => {
      console.log(res);
      if (res.success == true) {
        this.text = res.privacypolicy;
      } else {
      }
    });
  }
  getterms() {
    this.http.getApi(this.http.api.terms, false).then((res: any) => {
      console.log(res);
      if (res.success == true) {
        this.text = res.terms;
      } else {
      }
    });
  }
  goback() {
    this.location.back();
  }
  ngOnInit() {
    console.log("about page");
    this.route.queryParams.subscribe((params) => {
      this.name = params["name"];
    });
    if (this.name == "privacypolicy") {
      this.title = "Privacy Policy";
      this.getprivacy();
    }
    if (this.name == "terms") {
      this.title = "Terms & Conditions";
      this.getterms();
    }
  }
}
