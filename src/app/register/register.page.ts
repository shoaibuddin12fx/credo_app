import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { HttpService } from "../providers/http.service";
import { GeneralService } from "../providers/general.service";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobaldataService } from "../providers/globaldata.service";
import { Storage } from "@ionic/storage";
import { Events } from "../events";
import { NavigationExtras } from "@angular/router";
import { AlertController } from "@ionic/angular";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"],
  encapsulation: ViewEncapsulation.Emulated,
})
export class RegisterPage implements OnInit {
  public logoAnimation: string = "";
  public restAnimation: string = "";
  formgroup: FormGroup;
  name: AbstractControl;
  phone: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  check: boolean = false;
  isSubmitted: boolean = false;
  respone: any;
  constructor(
    private http: HttpService,
    private storage: Storage,
    public events: Events,
    private general: GeneralService,
    public formbuilder: FormBuilder,
    private router: Router
  ) {
    GlobaldataService.currentPage = "signup";
    let NAMEPATTERN = /^[^-\s][a-zA-Z_\s-]+$/i;
    let EMAILPATTERN = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/i;
    this.formgroup = formbuilder.group({
      name: ["", [Validators.required, Validators.pattern(NAMEPATTERN)]],
      phone: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.pattern(EMAILPATTERN)]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    });
    this.name = this.formgroup.controls["name"];
    this.phone = this.formgroup.controls["phone"];
    this.email = this.formgroup.controls["email"];
    this.password = this.formgroup.controls["password"];
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.formgroup.valid) {
      console.log("Please provide all the required values!");
      return false;
    }
    if (this.check == false) {
      this.general.presentToast(
        "Please check terms and condtions in order to proceed"
      );
    } else {
      console.log(this.formgroup.value);
      var service_data = {
        name: this.name.value,
        phone: this.phone.value,
        email: this.email.value,
        password: this.password.value,
        //dv_token:GlobaldataService.deviceToken
      };
      this.http
        .postApi2(this.http.api.signup, service_data, true)
        .then((res: any) => {
          console.log(res);
          if (res.success == true) {
            GlobaldataService.userObject = res.user;
            GlobaldataService.token = res.data.token;
            GlobaldataService.exhibitionstatus = res.exhibitionstatus;
            GlobaldataService.userName = res.user.name;
            this.storage.set("token", res.data.token);
            this.storage.set("userObject", res.user);
            this.general.presentToast(res.message);
            this.events.publish("user:created", {
              name: res.user.name,
              email: res.user.email,
              image: res.user.defaultavatar,
            });
            this.router.navigate(["tabs/projects"]);
          } else {
            this.general.presentToast(res.message);
          }
        });
    }
  }

  about(pagename) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        name: pagename,
      },
    };
    this.router.navigate(["about"], navigationExtras);
  }
  gotologin() {
    this.router.navigate(["login"]);
  }
  gotoselect() {
    this.router.navigate(["select"]);
  }
  applyAnimation() {
    this.logoAnimation = "animated " + "fadeInDown";
    this.restAnimation = "animated " + "fadeInUp";
  }

  ionViewWillEnter() {
    this.applyAnimation();
    console.log("signup page");
  }

  googlelogin() {
    this.general.presentAlert("Coming Soon", "Google login is coming soon");
  }

  fblogin() {
    this.general.presentAlert("Coming Soon", "Facebook login is coming soon");
  }

  ngOnInit() {}
}
