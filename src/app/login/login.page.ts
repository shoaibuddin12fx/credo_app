import { Component, OnInit } from "@angular/core";
import { HttpService } from "../providers/http.service";
import { GeneralService } from "../providers/general.service";
import { GlobaldataService } from "../providers/globaldata.service";
import { Storage } from "@ionic/storage";
import { Router, ActivatedRoute } from "@angular/router";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { Events } from "../events";
import { ModalController, MenuController } from "@ionic/angular";
import { NavigationExtras } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  public logoAnimation: string = "";
  public restAnimation: string = "";
  formgroup: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  check: boolean = false;
  isSubmitted: boolean = false;
  agree: boolean = true;
  fbuser: any;
  constructor(
    public menuCtrl: MenuController,
    private http: HttpService,
    private general: GeneralService,
    public formbuilder: FormBuilder,
    private storage: Storage,
    public events: Events,
    public modalController: ModalController,
    private router: Router
  ) {
    GlobaldataService.currentPage = "login";
    let EMAILPATTERN = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/i;
    this.formgroup = formbuilder.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });
    this.email = this.formgroup.controls["email"];
    this.password = this.formgroup.controls["password"];
    console.log("login page");
  }

  forgotpassword() {
    this.router.navigate(["forgot"]);
  }

  signup() {
    this.router.navigate(["register"]);
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.formgroup.valid) {
      console.log("Please provide all the required values!");
      return false;
    } else {
      console.log(this.formgroup.value);
      var service_data = {
        email: this.email.value,
        password: this.password.value,
        //dv_token:GlobaldataService.deviceToken
      };
      this.http
        .postApi2(this.http.api.login, service_data, true)
        .then((res: any) => {
          console.log(res);
          if (res.status == true) {
            GlobaldataService.userObject = res.data;
            GlobaldataService.userName = res.data.name;
            this.storage.set("userObject", res.data);

            this.events.publish("user:created", {
              name: res.data.name,
              email: res.data.email,
              // image: res.user.defaultavatar,
            });
            this.menuCtrl.enable(true);
            this.router.navigate(["projects"]);
          } else {
            this.general.presentToast(res.message);
          }
        });
    }
  }

  about(pagename) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        name: pagename,
      },
    };
    this.router.navigate(["about"], navigationExtras);
  }

  ionViewWillEnter() {}

  ngOnInit() {
    this.menuCtrl.enable(false);
  }
}
