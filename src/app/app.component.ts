import { Component, OnInit } from "@angular/core";
import { MenuController } from "@ionic/angular";
import {
  Platform,
  ToastController,
  ActionSheetController,
} from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { AlertController } from "@ionic/angular";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import { GlobaldataService } from "./providers/globaldata.service";
import { Events } from "../app/events";
//import { NetworkService } from "../app/network";
import { Network } from "@ionic-native/network/ngx";
import { FCM } from "@ionic-native/fcm/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { HttpService } from "./providers/http.service";
import { NavigationExtras } from "@angular/router";
import { NgZone } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit {
  selectedIndex: any;
  name: any;
  email: any;
  image: any;
  SDK_KEY: "";
  SDK_SECRET: "";
  msgtoshare: any;
  public onlineOffline: boolean = window.navigator.onLine;
  public appPages = [
    {
      title: "Home",
      url: "projects",
      image: "assets/images/dashboard.png",
      imageactive: "assets/images/dashboardwhite.png",
    },
  ];
  public labels = ["Family", "Friends", "Notes", "Work", "Travel", "Reminders"];

  constructor(
    public actionSheetController: ActionSheetController,
    private router: Router,
    private androidPermissions: AndroidPermissions,
    private storage: Storage,
    public menuCtrl: MenuController,
    private platform: Platform,
    private splashScreen: SplashScreen,
    public events: Events,
    private statusBar: StatusBar,
    public alertController: AlertController,
    public toastCtrl: ToastController,
    private network: Network,
    private http: HttpService,
    private zone: NgZone,
    private fcm: FCM
  ) {
    this.initializeApp();
    this.events.subscribe("user:created", (data: any) => {
      this.name = data.name;
      this.email = data.email;
      this.image = data.image;
      console.log("Welcome", data.name, "at", data.email, data.image);
    });
    // this.events.subscribe("zoom:created", (data: any) => {
    //   this.SDK_KEY = data.sdkkey;
    //   this.SDK_SECRET = data.sdksecret;
    //   this.initializeZoom(this.SDK_KEY, this.SDK_SECRET);
    // });
    this.events.subscribe("updatepage", (data: any) => {
      this.selectedIndex = data.index;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        GlobaldataService.platform = "android";
      } else if (this.platform.is("ios")) {
        GlobaldataService.platform = "ios";
      }
      this.selectedIndex = 0;
      this.statusBar.styleDefault();
      this.statusBar.styleBlackTranslucent();
      this.statusBar.backgroundColorByHexString("#016ef6");
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);

      if (GlobaldataService.platform == "android") {
        this.androidPermissions
          .checkPermission(
            this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE
          )
          .then(
            (result) => {
              if (result.hasPermission) {
                // code
              } else {
                this.androidPermissions
                  .requestPermission(
                    this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE
                  )
                  .then((result) => {
                    if (result.hasPermission) {
                      // code
                    }
                  });
              }
            },
            (err) =>
              this.androidPermissions.hasPermission(
                this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE
              )
          );

        this.androidPermissions
          .checkPermission(
            this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
          )
          .then(
            (result) => {
              if (result.hasPermission) {
                // code
              } else {
                this.androidPermissions
                  .requestPermission(
                    this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
                  )
                  .then((result) => {
                    if (result.hasPermission) {
                      // code
                    }
                  });
              }
            },
            (err) =>
              this.androidPermissions.hasPermission(
                this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
              )
          );
      }

      // watch network for a disconnection
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        console.log("network was disconnected :-(");
        this.router.navigate(["/nointernet"]);
      });

      // stop disconnect watch

      // watch network for a connection
      let connectSubscription = this.network.onConnect().subscribe(() => {
        console.log("network connected!");
        if (GlobaldataService.netcheck == true) {
          this.initializeApp();
        }
        // We just got a connection but we need to wait briefly
        // before we determine the connection type. Might need to wait.
        // prior to doing any api requests as well.
        setTimeout(() => {
          if (this.network.type === "wifi") {
            console.log("we got a wifi connection, woohoo!");
          }
        }, 3000);
      });

      // stop connect watch
      this.checkLogin();
    });
  }

  logout() {
    this.storage.clear();
    GlobaldataService.isloggedin = false;
    this.router.navigate(["/login"]);
  }

  pushSetup() {
    this.fcm.getToken().then((token) => {
      console.log(token);
      GlobaldataService.deviceToken = token;
    });

    this.fcm.onNotification().subscribe((data) => {
      if (data.wasTapped) {
        console.log("Received in background");
      } else {
        console.log("Received in foreground");
      }
    });
  }

  about(pagename) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        name: pagename,
      },
    };
    this.router.navigate(["about"], navigationExtras);
  }
  profile(pagename) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        name: pagename,
      },
    };
    this.router.navigate(["profile"], navigationExtras);
  }
  selectCounsellor(pagename) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        name: pagename,
      },
    };
    this.router.navigate(["selectCounsellor"], navigationExtras);
  }


  // sharefb(){
  //   // this.socialSharing.
  // }

  checkLogin() {
    // this.pushSetup();
    this.storage.get("userObject").then((val) => {
      console.log("login data", val);
      if (val != null) {
        GlobaldataService.userObject = val;
        this.name = GlobaldataService.userObject.name;
        this.email = GlobaldataService.userObject.email;
        this.image = GlobaldataService.userObject.defaultavatar;
        GlobaldataService.isloggedin = true;
        GlobaldataService.userName = this.name;
        this.storage.get("token").then((val2) => {
          if (val2 != null) {
            GlobaldataService.token = val2;
            this.router.navigate(["/projects"]);
            //this.router.navigate(["/promotionalmaterial"]);
          }
        });
      } else {
        this.router.navigate(["/login"]);
        //this.router.navigate(["promotionalmaterial"]);

        GlobaldataService.isloggedin = false;
      }
    });
  }

  editprofile() {
    this.router.navigate(["/editprofile"]);
    this.menuCtrl.toggle();
  }

  ngOnInit() {
    console.log("pages");
    const path = window.location.pathname.split("folder/")[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(
        (page) => page.title.toLowerCase() === path.toLowerCase()
      );
    }
  }

  async presentToast(text) {
    const toast = await this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "top",
    });
    toast.present();
  }
}
