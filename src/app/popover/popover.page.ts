import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController, PopoverController } from '@ionic/angular';
import { OverviewPage } from '../overview/overview.page';
import { NavService } from '../service/nav.service';
@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {
  [x: string]: any;
  constructor(public popoverController: PopoverController, public router: Router, public nav: NavService,) { }

  ngOnInit() { }

  selectCounsellor($event) {
    console.log($event);
    this.popoverController.dismiss({ key: 'selectCounsellor' });
  }
  overView($event) {
    console.log($event)
    this.popoverController.dismiss({ key: 'overview' });
  }
  showProfile($event) {
    console.log($event)
    this.popoverController.dismiss({ key: 'profile' });
  }
  close() {
    this.popoverController.dismiss({ key: '' });
  }
}
